"""
# coding:utf-8
@Time    : 2021/08/30
@Author  : jiangwei
@mail    : qq804022023@gmail.com
@File    : utils.py
@Desc    : utils
@Software: PyCharm
"""
import urllib.parse
import hmac
import base64
import hashlib
import requests
from webhook_payload import pr_merge_payload, issue_open_payload
import json

secret = 'kylinos123'
GITEE_TOKEN = '4ee6d6f8208ca9f3abc88801eeadad30'
SEND_PR_COMMENT_URL = 'https://gitee.com/api/v5/repos/{owner}/{repo}/pulls/{number}/comments'
ADD_PR_LABEL_URL = 'https://gitee.com/api/v5/repos/{owner}/{repo}/pulls/{number}/labels'
SEND_ISSUE_COMMENT_URL = 'https://gitee.com/api/v5/repos/{owner}/{repo}/issues/{number}/comments'
HEADERS = {'Content-Type': 'application/json;charset=UTF-8'}


def check_sign(timestamp, sign):
    """
    check web hook sign
    :param timestamp: web hook timestamp
    :param sign: web hook sign
    :return: check result sign == check_sign
    """
    string_to_sign = '{}\n{}'.format(timestamp, secret)
    secret_enc = secret.encode('utf-8')
    string_to_sign_enc = string_to_sign.encode('utf-8')
    hmac_code = hmac.new(secret_enc, string_to_sign_enc, digestmod=hashlib.sha256).digest()
    check_sign = bytes.decode(base64.b64encode(hmac_code), encoding='utf-8')
    return check_sign == sign


def send_greeting_for_pr(datas: dict):
    pr_user = datas.get('pull_request').get('user').get('username')
    repo_name = datas.get('target_repo').get('repository').get('name')
    repo_owner = datas.get('target_repo').get('repository').get('owner').get('username')
    pr_number = datas.get('pull_request').get('number')
    pr_url = SEND_PR_COMMENT_URL.format(owner=repo_owner, repo=repo_name, number=pr_number)
    body = "Hey <b>{}</b>,welcome to the openSource Community.\nI'm the Bot here serving you. You can find the instructions on how to interact with me at\n https://gitee.com/openeuler/community/blob/master/en/sig-infrastructure/command.md.".format(
        pr_user)
    payload = {'access_token': GITEE_TOKEN,
               'body': body}

    res = requests.post(pr_url, headers=HEADERS, json=payload)
    return res


def add_label_for_merge(datas: dict):
    pr_number = datas.get('pull_request').get('number')
    pr_user = datas.get('pull_request').get('user').get('username')
    repo_name = datas.get('target_repo').get('repository').get('name')
    repo_owner = datas.get('target_repo').get('repository').get('owner').get('username')
    label_url = ADD_PR_LABEL_URL.format(owner=repo_owner, repo=repo_name, number=pr_number) + '?access_token={}'.format(GITEE_TOKEN)
    pr_url = SEND_PR_COMMENT_URL.format(owner=repo_owner, repo=repo_name, number=pr_number)
    body = ['merged', 'approved', 'abc']
    payload = {'access_token': GITEE_TOKEN,
               'body': body}
    requests.post(label_url, headers=HEADERS, json=body)
    body = "Hey <b>{}</b>, Thanks for your contribution, this pull request was merged.".format(pr_user)
    payload['body'] = body
    res = requests.post(pr_url, headers=HEADERS, json=payload)
    return res


def send_greeting_for_issue(datas: dict):
    issue_number = datas.get('issue').get('number')
    repo_name = datas.get('repository').get('name')
    repo_owner = datas.get('repository').get('owner').get('username')
    issue_user = datas.get('sender').get('name')
    payload = {"access_token": GITEE_TOKEN,
               'body': 'Hello, <b>{}</b>, I am a service robot, thank you for your feedback, we will resolve it as soon as possible.'.format(issue_user)}
    issue_url = SEND_ISSUE_COMMENT_URL.format(owner=repo_owner, repo=repo_name, number=issue_number)
    res = requests.post(issue_url, json=payload)
    print(res.text)
    return res


datas = {
    "action": "test",
    "action_desc": None,
    "pull_request": {
        "id": 4529154,
        "number": 3,
        "state": "open",
        "html_url": "https://gitee.com/xueyang721/test-webhook/pulls/3",
        "diff_url": "https://gitee.com/xueyang721/test-webhook/pulls/3.diff",
        "patch_url": "https://gitee.com/xueyang721/test-webhook/pulls/3.patch",
        "title": "更新reademe.md文件",
        "body": "1. 新增简单的软件说明\r\n2. 新增安装说明",
        "labels": [

        ],
        "languages": [

        ],
        "created_at": "2021-08-30T17:58:51+08:00",
        "updated_at": "2021-08-30T17:58:52+08:00",
        "closed_at": None,
        "merged_at": None,
        "merge_commit_sha": "613f5d02b0bf074fd77f80c5f5fa53be8858067d",
        "merge_reference_name": "refs/pull/3/MERGE",
        "user": {
            "id": 9637903,
            "name": "jiangwei",
            "email": "qq804022023@gmail.com",
            "username": "jiangwei124",
            "user_name": "jiangwei124",
            "url": "https://gitee.com/jiangwei124",
            "login": "jiangwei124",
            "avatar_url": "https://portrait.gitee.com/uploads/avatars/user/3212/9637903_jiangwei124_1629964158.png",
            "html_url": "https://gitee.com/jiangwei124",
            "type": "User",
            "site_admin": False
        },
        "assignee": None,
        "assignees": [

        ],
        "tester": None,
        "testers": [

        ],
        "need_test": True,
        "need_review": True,
        "milestone": None,
        "head": {
            "label": "jiangwei:master",
            "ref": "master",
            "sha": "f3b66a50a4bd2bc1dae7fd44d669741deaea74ee",
            "user": {
                "id": 9637903,
                "name": "jiangwei",
                "email": "qq804022023@gmail.com",
                "username": "jiangwei124",
                "user_name": "jiangwei124",
                "url": "https://gitee.com/jiangwei124",
                "login": "jiangwei124",
                "avatar_url": "https://portrait.gitee.com/uploads/avatars/user/3212/9637903_jiangwei124_1629964158.png",
                "html_url": "https://gitee.com/jiangwei124",
                "type": "User",
                "site_admin": False
            },
            "repo": {
                "id": 17779717,
                "name": "test-webhook",
                "path": "test-webhook",
                "full_name": "jiangwei124/test-webhook",
                "owner": {
                    "id": 9637903,
                    "name": "jiangwei",
                    "email": "qq804022023@gmail.com",
                    "username": "jiangwei124",
                    "user_name": "jiangwei124",
                    "url": "https://gitee.com/jiangwei124",
                    "login": "jiangwei124",
                    "avatar_url": "https://portrait.gitee.com/uploads/avatars/user/3212/9637903_jiangwei124_1629964158.png",
                    "html_url": "https://gitee.com/jiangwei124",
                    "type": "User",
                    "site_admin": False
                },
                "private": False,
                "html_url": "https://gitee.com/jiangwei124/test-webhook",
                "url": "https://gitee.com/jiangwei124/test-webhook",
                "description": "测试webhook",
                "fork": True,
                "created_at": "2021-08-30T17:41:38+08:00",
                "updated_at": "2021-08-30T17:43:37+08:00",
                "pushed_at": "2021-08-30T17:43:37+08:00",
                "git_url": "git://gitee.com/jiangwei124/test-webhook.git",
                "ssh_url": "git@gitee.com:jiangwei124/test-webhook.git",
                "clone_url": "https://gitee.com/jiangwei124/test-webhook.git",
                "svn_url": "svn://gitee.com/jiangwei124/test-webhook",
                "git_http_url": "https://gitee.com/jiangwei124/test-webhook.git",
                "git_ssh_url": "git@gitee.com:jiangwei124/test-webhook.git",
                "git_svn_url": "svn://gitee.com/jiangwei124/test-webhook",
                "homepage": None,
                "stargazers_count": 0,
                "watchers_count": 1,
                "forks_count": 0,
                "language": "Python",
                "has_issues": True,
                "has_wiki": True,
                "has_pages": False,
                "license": "Apache-2.0",
                "open_issues_count": 0,
                "default_branch": "master",
                "namespace": "jiangwei124",
                "name_with_namespace": "jiangwei/test-webhook",
                "path_with_namespace": "jiangwei124/test-webhook"
            }
        },
        "base": {
            "label": "xueyang:master",
            "ref": "master",
            "sha": "67a96fcbb6d70e518efc87b04ae64357c08896e0",
            "user": {
                "id": 9639339,
                "name": "xueyang",
                "email": "913538009@qq.com",
                "username": "xueyang721",
                "user_name": "xueyang721",
                "url": "https://gitee.com/xueyang721",
                "login": "xueyang721",
                "avatar_url": "https://gitee.com/assets/no_portrait.png",
                "html_url": "https://gitee.com/xueyang721",
                "type": "User",
                "site_admin": False
            },
            "repo": {
                "id": 17773178,
                "name": "test-webhook",
                "path": "test-webhook",
                "full_name": "xueyang721/test-webhook",
                "owner": {
                    "id": 9639339,
                    "name": "xueyang",
                    "email": "913538009@qq.com",
                    "username": "xueyang721",
                    "user_name": "xueyang721",
                    "url": "https://gitee.com/xueyang721",
                    "login": "xueyang721",
                    "avatar_url": "https://gitee.com/assets/no_portrait.png",
                    "html_url": "https://gitee.com/xueyang721",
                    "type": "User",
                    "site_admin": False
                },
                "private": False,
                "html_url": "https://gitee.com/xueyang721/test-webhook",
                "url": "https://gitee.com/xueyang721/test-webhook",
                "description": "测试webhook",
                "fork": False,
                "created_at": "2021-08-30T13:58:30+08:00",
                "updated_at": "2021-08-30T17:57:52+08:00",
                "pushed_at": "2021-08-30T17:40:18+08:00",
                "git_url": "git://gitee.com/xueyang721/test-webhook.git",
                "ssh_url": "git@gitee.com:xueyang721/test-webhook.git",
                "clone_url": "https://gitee.com/xueyang721/test-webhook.git",
                "svn_url": "svn://gitee.com/xueyang721/test-webhook",
                "git_http_url": "https://gitee.com/xueyang721/test-webhook.git",
                "git_ssh_url": "git@gitee.com:xueyang721/test-webhook.git",
                "git_svn_url": "svn://gitee.com/xueyang721/test-webhook",
                "homepage": None,
                "stargazers_count": 0,
                "watchers_count": 1,
                "forks_count": 1,
                "language": "Python",
                "has_issues": True,
                "has_wiki": True,
                "has_pages": False,
                "license": "Apache-2.0",
                "open_issues_count": 1,
                "default_branch": "master",
                "namespace": "xueyang721",
                "name_with_namespace": "xueyang/test-webhook",
                "path_with_namespace": "xueyang721/test-webhook"
            }
        },
        "merged": False,
        "mergeable": True,
        "merge_status": "can_be_merged",
        "updated_by": {
            "id": 9637903,
            "name": "jiangwei",
            "email": "qq804022023@gmail.com",
            "username": "jiangwei124",
            "user_name": "jiangwei124",
            "url": "https://gitee.com/jiangwei124",
            "login": "jiangwei124",
            "avatar_url": "https://portrait.gitee.com/uploads/avatars/user/3212/9637903_jiangwei124_1629964158.png",
            "html_url": "https://gitee.com/jiangwei124",
            "type": "User",
            "site_admin": False
        },
        "comments": 0,
        "commits": 1,
        "additions": 8,
        "deletions": 22,
        "changed_files": 1
    },
    "number": 3,
    "iid": 3,
    "title": "更新reademe.md文件",
    "body": "1. 新增简单的软件说明\r\n2. 新增安装说明",
    "languages": [

    ],
    "state": "open",
    "merge_status": "can_be_merged",
    "merge_commit_sha": "613f5d02b0bf074fd77f80c5f5fa53be8858067d",
    "url": "https://gitee.com/xueyang721/test-webhook/pulls/3",
    "source_branch": "master",
    "source_repo": {
        "project": {
            "id": 17779717,
            "name": "test-webhook",
            "path": "test-webhook",
            "full_name": "jiangwei124/test-webhook",
            "owner": {
                "id": 9637903,
                "name": "jiangwei",
                "email": "qq804022023@gmail.com",
                "username": "jiangwei124",
                "user_name": "jiangwei124",
                "url": "https://gitee.com/jiangwei124",
                "login": "jiangwei124",
                "avatar_url": "https://portrait.gitee.com/uploads/avatars/user/3212/9637903_jiangwei124_1629964158.png",
                "html_url": "https://gitee.com/jiangwei124",
                "type": "User",
                "site_admin": False
            },
            "private": False,
            "html_url": "https://gitee.com/jiangwei124/test-webhook",
            "url": "https://gitee.com/jiangwei124/test-webhook",
            "description": "测试webhook",
            "fork": True,
            "created_at": "2021-08-30T17:41:38+08:00",
            "updated_at": "2021-08-30T17:43:37+08:00",
            "pushed_at": "2021-08-30T17:43:37+08:00",
            "git_url": "git://gitee.com/jiangwei124/test-webhook.git",
            "ssh_url": "git@gitee.com:jiangwei124/test-webhook.git",
            "clone_url": "https://gitee.com/jiangwei124/test-webhook.git",
            "svn_url": "svn://gitee.com/jiangwei124/test-webhook",
            "git_http_url": "https://gitee.com/jiangwei124/test-webhook.git",
            "git_ssh_url": "git@gitee.com:jiangwei124/test-webhook.git",
            "git_svn_url": "svn://gitee.com/jiangwei124/test-webhook",
            "homepage": None,
            "stargazers_count": 0,
            "watchers_count": 1,
            "forks_count": 0,
            "language": "Python",
            "has_issues": True,
            "has_wiki": True,
            "has_pages": False,
            "license": "Apache-2.0",
            "open_issues_count": 0,
            "default_branch": "master",
            "namespace": "jiangwei124",
            "name_with_namespace": "jiangwei/test-webhook",
            "path_with_namespace": "jiangwei124/test-webhook"
        },
        "repository": {
            "id": 17779717,
            "name": "test-webhook",
            "path": "test-webhook",
            "full_name": "jiangwei124/test-webhook",
            "owner": {
                "id": 9637903,
                "name": "jiangwei",
                "email": "qq804022023@gmail.com",
                "username": "jiangwei124",
                "user_name": "jiangwei124",
                "url": "https://gitee.com/jiangwei124",
                "login": "jiangwei124",
                "avatar_url": "https://portrait.gitee.com/uploads/avatars/user/3212/9637903_jiangwei124_1629964158.png",
                "html_url": "https://gitee.com/jiangwei124",
                "type": "User",
                "site_admin": False
            },
            "private": False,
            "html_url": "https://gitee.com/jiangwei124/test-webhook",
            "url": "https://gitee.com/jiangwei124/test-webhook",
            "description": "测试webhook",
            "fork": True,
            "created_at": "2021-08-30T17:41:38+08:00",
            "updated_at": "2021-08-30T17:43:37+08:00",
            "pushed_at": "2021-08-30T17:43:37+08:00",
            "git_url": "git://gitee.com/jiangwei124/test-webhook.git",
            "ssh_url": "git@gitee.com:jiangwei124/test-webhook.git",
            "clone_url": "https://gitee.com/jiangwei124/test-webhook.git",
            "svn_url": "svn://gitee.com/jiangwei124/test-webhook",
            "git_http_url": "https://gitee.com/jiangwei124/test-webhook.git",
            "git_ssh_url": "git@gitee.com:jiangwei124/test-webhook.git",
            "git_svn_url": "svn://gitee.com/jiangwei124/test-webhook",
            "homepage": None,
            "stargazers_count": 0,
            "watchers_count": 1,
            "forks_count": 0,
            "language": "Python",
            "has_issues": True,
            "has_wiki": True,
            "has_pages": False,
            "license": "Apache-2.0",
            "open_issues_count": 0,
            "default_branch": "master",
            "namespace": "jiangwei124",
            "name_with_namespace": "jiangwei/test-webhook",
            "path_with_namespace": "jiangwei124/test-webhook"
        }
    },
    "target_branch": "master",
    "target_repo": {
        "project": {
            "id": 17773178,
            "name": "test-webhook",
            "path": "test-webhook",
            "full_name": "xueyang721/test-webhook",
            "owner": {
                "id": 9639339,
                "name": "xueyang",
                "email": "913538009@qq.com",
                "username": "xueyang721",
                "user_name": "xueyang721",
                "url": "https://gitee.com/xueyang721",
                "login": "xueyang721",
                "avatar_url": "https://gitee.com/assets/no_portrait.png",
                "html_url": "https://gitee.com/xueyang721",
                "type": "User",
                "site_admin": False
            },
            "private": False,
            "html_url": "https://gitee.com/xueyang721/test-webhook",
            "url": "https://gitee.com/xueyang721/test-webhook",
            "description": "测试webhook",
            "fork": False,
            "created_at": "2021-08-30T13:58:30+08:00",
            "updated_at": "2021-08-30T17:57:52+08:00",
            "pushed_at": "2021-08-30T17:40:18+08:00",
            "git_url": "git://gitee.com/xueyang721/test-webhook.git",
            "ssh_url": "git@gitee.com:xueyang721/test-webhook.git",
            "clone_url": "https://gitee.com/xueyang721/test-webhook.git",
            "svn_url": "svn://gitee.com/xueyang721/test-webhook",
            "git_http_url": "https://gitee.com/xueyang721/test-webhook.git",
            "git_ssh_url": "git@gitee.com:xueyang721/test-webhook.git",
            "git_svn_url": "svn://gitee.com/xueyang721/test-webhook",
            "homepage": None,
            "stargazers_count": 0,
            "watchers_count": 1,
            "forks_count": 1,
            "language": "Python",
            "has_issues": True,
            "has_wiki": True,
            "has_pages": False,
            "license": "Apache-2.0",
            "open_issues_count": 1,
            "default_branch": "master",
            "namespace": "xueyang721",
            "name_with_namespace": "xueyang/test-webhook",
            "path_with_namespace": "xueyang721/test-webhook"
        },
        "repository": {
            "id": 17773178,
            "name": "test-webhook",
            "path": "test-webhook",
            "full_name": "xueyang721/test-webhook",
            "owner": {
                "id": 9639339,
                "name": "xueyang",
                "email": "913538009@qq.com",
                "username": "xueyang721",
                "user_name": "xueyang721",
                "url": "https://gitee.com/xueyang721",
                "login": "xueyang721",
                "avatar_url": "https://gitee.com/assets/no_portrait.png",
                "html_url": "https://gitee.com/xueyang721",
                "type": "User",
                "site_admin": False
            },
            "private": False,
            "html_url": "https://gitee.com/xueyang721/test-webhook",
            "url": "https://gitee.com/xueyang721/test-webhook",
            "description": "测试webhook",
            "fork": False,
            "created_at": "2021-08-30T13:58:30+08:00",
            "updated_at": "2021-08-30T17:57:52+08:00",
            "pushed_at": "2021-08-30T17:40:18+08:00",
            "git_url": "git://gitee.com/xueyang721/test-webhook.git",
            "ssh_url": "git@gitee.com:xueyang721/test-webhook.git",
            "clone_url": "https://gitee.com/xueyang721/test-webhook.git",
            "svn_url": "svn://gitee.com/xueyang721/test-webhook",
            "git_http_url": "https://gitee.com/xueyang721/test-webhook.git",
            "git_ssh_url": "git@gitee.com:xueyang721/test-webhook.git",
            "git_svn_url": "svn://gitee.com/xueyang721/test-webhook",
            "homepage": None,
            "stargazers_count": 0,
            "watchers_count": 1,
            "forks_count": 1,
            "language": "Python",
            "has_issues": True,
            "has_wiki": True,
            "has_pages": False,
            "license": "Apache-2.0",
            "open_issues_count": 1,
            "default_branch": "master",
            "namespace": "xueyang721",
            "name_with_namespace": "xueyang/test-webhook",
            "path_with_namespace": "xueyang721/test-webhook"
        }
    },
    "project": {
        "id": 17773178,
        "name": "test-webhook",
        "path": "test-webhook",
        "full_name": "xueyang721/test-webhook",
        "owner": {
            "id": 9639339,
            "name": "xueyang",
            "email": "913538009@qq.com",
            "username": "xueyang721",
            "user_name": "xueyang721",
            "url": "https://gitee.com/xueyang721",
            "login": "xueyang721",
            "avatar_url": "https://gitee.com/assets/no_portrait.png",
            "html_url": "https://gitee.com/xueyang721",
            "type": "User",
            "site_admin": False
        },
        "private": False,
        "html_url": "https://gitee.com/xueyang721/test-webhook",
        "url": "https://gitee.com/xueyang721/test-webhook",
        "description": "测试webhook",
        "fork": False,
        "created_at": "2021-08-30T13:58:30+08:00",
        "updated_at": "2021-08-30T17:57:52+08:00",
        "pushed_at": "2021-08-30T17:40:18+08:00",
        "git_url": "git://gitee.com/xueyang721/test-webhook.git",
        "ssh_url": "git@gitee.com:xueyang721/test-webhook.git",
        "clone_url": "https://gitee.com/xueyang721/test-webhook.git",
        "svn_url": "svn://gitee.com/xueyang721/test-webhook",
        "git_http_url": "https://gitee.com/xueyang721/test-webhook.git",
        "git_ssh_url": "git@gitee.com:xueyang721/test-webhook.git",
        "git_svn_url": "svn://gitee.com/xueyang721/test-webhook",
        "homepage": None,
        "stargazers_count": 0,
        "watchers_count": 1,
        "forks_count": 1,
        "language": "Python",
        "has_issues": True,
        "has_wiki": True,
        "has_pages": False,
        "license": "Apache-2.0",
        "open_issues_count": 1,
        "default_branch": "master",
        "namespace": "xueyang721",
        "name_with_namespace": "xueyang/test-webhook",
        "path_with_namespace": "xueyang721/test-webhook"
    },
    "repository": {
        "id": 17773178,
        "name": "test-webhook",
        "path": "test-webhook",
        "full_name": "xueyang721/test-webhook",
        "owner": {
            "id": 9639339,
            "name": "xueyang",
            "email": "913538009@qq.com",
            "username": "xueyang721",
            "user_name": "xueyang721",
            "url": "https://gitee.com/xueyang721",
            "login": "xueyang721",
            "avatar_url": "https://gitee.com/assets/no_portrait.png",
            "html_url": "https://gitee.com/xueyang721",
            "type": "User",
            "site_admin": False
        },
        "private": False,
        "html_url": "https://gitee.com/xueyang721/test-webhook",
        "url": "https://gitee.com/xueyang721/test-webhook",
        "description": "测试webhook",
        "fork": False,
        "created_at": "2021-08-30T13:58:30+08:00",
        "updated_at": "2021-08-30T17:57:52+08:00",
        "pushed_at": "2021-08-30T17:40:18+08:00",
        "git_url": "git://gitee.com/xueyang721/test-webhook.git",
        "ssh_url": "git@gitee.com:xueyang721/test-webhook.git",
        "clone_url": "https://gitee.com/xueyang721/test-webhook.git",
        "svn_url": "svn://gitee.com/xueyang721/test-webhook",
        "git_http_url": "https://gitee.com/xueyang721/test-webhook.git",
        "git_ssh_url": "git@gitee.com:xueyang721/test-webhook.git",
        "git_svn_url": "svn://gitee.com/xueyang721/test-webhook",
        "homepage": None,
        "stargazers_count": 0,
        "watchers_count": 1,
        "forks_count": 1,
        "language": "Python",
        "has_issues": True,
        "has_wiki": True,
        "has_pages": False,
        "license": "Apache-2.0",
        "open_issues_count": 1,
        "default_branch": "master",
        "namespace": "xueyang721",
        "name_with_namespace": "xueyang/test-webhook",
        "path_with_namespace": "xueyang721/test-webhook"
    },
    "author": {
        "id": 9637903,
        "name": "jiangwei",
        "email": "qq804022023@gmail.com",
        "username": "jiangwei124",
        "user_name": "jiangwei124",
        "url": "https://gitee.com/jiangwei124",
        "login": "jiangwei124",
        "avatar_url": "https://portrait.gitee.com/uploads/avatars/user/3212/9637903_jiangwei124_1629964158.png",
        "html_url": "https://gitee.com/jiangwei124",
        "type": "User",
        "site_admin": False
    },
    "updated_by": {
        "id": 9637903,
        "name": "jiangwei",
        "email": "qq804022023@gmail.com",
        "username": "jiangwei124",
        "user_name": "jiangwei124",
        "url": "https://gitee.com/jiangwei124",
        "login": "jiangwei124",
        "avatar_url": "https://portrait.gitee.com/uploads/avatars/user/3212/9637903_jiangwei124_1629964158.png",
        "html_url": "https://gitee.com/jiangwei124",
        "type": "User",
        "site_admin": False
    },
    "sender": {
        "id": 9637903,
        "name": "jiangwei",
        "email": "qq804022023@gmail.com",
        "username": "jiangwei124",
        "user_name": "jiangwei124",
        "url": "https://gitee.com/jiangwei124",
        "login": "jiangwei124",
        "avatar_url": "https://portrait.gitee.com/uploads/avatars/user/3212/9637903_jiangwei124_1629964158.png",
        "html_url": "https://gitee.com/jiangwei124",
        "type": "User",
        "site_admin": False
    },
    "target_user": {
        "id": 9639339,
        "name": "xueyang",
        "email": "913538009@qq.com",
        "username": "xueyang721",
        "user_name": "xueyang721",
        "url": "https://gitee.com/xueyang721"
    },
    "enterprise": None,
    "hook_name": "merge_request_hooks",
    "hook_id": 735943,
    "hook_url": "https://gitee.com/xueyang721/test-webhook/hooks/735943/edit",
    "password": "",
    "timestamp": "1630317532514",
    "sign": "PZqagNg/NKZUY1Urij7qfZBXCdcJXK0qX9npJgPwFW0="
}

issue = {
  "action": "open",
  "issue": {
    "html_url": "https://gitee.com/xueyang721/test-webhook/issues/I480IG",
    "id": 7092376,
    "number": "I480IG",
    "title": "测试一下webhook的issue",
    "user": {
      "id": 9637903,
      "name": "jiangwei",
      "email": "qq804022023@gmail.com",
      "username": "jiangwei124",
      "user_name": "jiangwei124",
      "url": "https://gitee.com/jiangwei124",
      "login": "jiangwei124",
      "avatar_url": "https://portrait.gitee.com/uploads/avatars/user/3212/9637903_jiangwei124_1629964158.png",
      "html_url": "https://gitee.com/jiangwei124",
      "type": "User",
      "site_admin": False
    },
    "labels": [

    ],
    "state": "open",
    "state_name": "待办的",
    "type_name": "任务",
    "assignee": None,
    "collaborators": [

    ],
    "milestone": None,
    "comments": 0,
    "created_at": "2021-08-31T15:58:53+08:00",
    "updated_at": "2021-08-31T15:58:53+08:00",
    "body": "测试一下"
  },
  "repository": {
    "id": 17773178,
    "name": "test-webhook",
    "path": "test-webhook",
    "full_name": "xueyang721/test-webhook",
    "owner": {
      "id": 9639339,
      "name": "xueyang",
      "email": "913538009@qq.com",
      "username": "xueyang721",
      "user_name": "xueyang721",
      "url": "https://gitee.com/xueyang721",
      "login": "xueyang721",
      "avatar_url": "https://gitee.com/assets/no_portrait.png",
      "html_url": "https://gitee.com/xueyang721",
      "type": "User",
      "site_admin": False
    },
    "private": False,
    "html_url": "https://gitee.com/xueyang721/test-webhook",
    "url": "https://gitee.com/xueyang721/test-webhook",
    "description": "测试webhook",
    "fork": False,
    "created_at": "2021-08-30T13:58:30+08:00",
    "updated_at": "2021-08-31T15:51:55+08:00",
    "pushed_at": "2021-08-31T11:09:34+08:00",
    "git_url": "git://gitee.com/xueyang721/test-webhook.git",
    "ssh_url": "git@gitee.com:xueyang721/test-webhook.git",
    "clone_url": "https://gitee.com/xueyang721/test-webhook.git",
    "svn_url": "svn://gitee.com/xueyang721/test-webhook",
    "git_http_url": "https://gitee.com/xueyang721/test-webhook.git",
    "git_ssh_url": "git@gitee.com:xueyang721/test-webhook.git",
    "git_svn_url": "svn://gitee.com/xueyang721/test-webhook",
    "homepage": "",
    "stargazers_count": 0,
    "watchers_count": 1,
    "forks_count": 1,
    "language": "Python",
    "has_issues": True,
    "has_wiki": True,
    "has_pages": False,
    "license": "Apache-2.0",
    "open_issues_count": 5,
    "default_branch": "master",
    "namespace": "xueyang721",
    "name_with_namespace": "xueyang/test-webhook",
    "path_with_namespace": "xueyang721/test-webhook"
  },
  "project": {
    "id": 17773178,
    "name": "test-webhook",
    "path": "test-webhook",
    "full_name": "xueyang721/test-webhook",
    "owner": {
      "id": 9639339,
      "name": "xueyang",
      "email": "913538009@qq.com",
      "username": "xueyang721",
      "user_name": "xueyang721",
      "url": "https://gitee.com/xueyang721",
      "login": "xueyang721",
      "avatar_url": "https://gitee.com/assets/no_portrait.png",
      "html_url": "https://gitee.com/xueyang721",
      "type": "User",
      "site_admin": False
    },
    "private": False,
    "html_url": "https://gitee.com/xueyang721/test-webhook",
    "url": "https://gitee.com/xueyang721/test-webhook",
    "description": "测试webhook",
    "fork": False,
    "created_at": "2021-08-30T13:58:30+08:00",
    "updated_at": "2021-08-31T15:51:55+08:00",
    "pushed_at": "2021-08-31T11:09:34+08:00",
    "git_url": "git://gitee.com/xueyang721/test-webhook.git",
    "ssh_url": "git@gitee.com:xueyang721/test-webhook.git",
    "clone_url": "https://gitee.com/xueyang721/test-webhook.git",
    "svn_url": "svn://gitee.com/xueyang721/test-webhook",
    "git_http_url": "https://gitee.com/xueyang721/test-webhook.git",
    "git_ssh_url": "git@gitee.com:xueyang721/test-webhook.git",
    "git_svn_url": "svn://gitee.com/xueyang721/test-webhook",
    "homepage": "",
    "stargazers_count": 0,
    "watchers_count": 1,
    "forks_count": 1,
    "language": "Python",
    "has_issues": True,
    "has_wiki": True,
    "has_pages": False,
    "license": "Apache-2.0",
    "open_issues_count": 5,
    "default_branch": "master",
    "namespace": "xueyang721",
    "name_with_namespace": "xueyang/test-webhook",
    "path_with_namespace": "xueyang721/test-webhook"
  },
  "sender": {
    "id": 9637903,
    "name": "jiangwei",
    "email": "qq804022023@gmail.com",
    "username": "jiangwei124",
    "user_name": "jiangwei124",
    "url": "https://gitee.com/jiangwei124",
    "login": "jiangwei124",
    "avatar_url": "https://portrait.gitee.com/uploads/avatars/user/3212/9637903_jiangwei124_1629964158.png",
    "html_url": "https://gitee.com/jiangwei124",
    "type": "User",
    "site_admin": False
  },
  "target_user": None,
  "user": {
    "id": 9637903,
    "name": "jiangwei",
    "email": "qq804022023@gmail.com",
    "username": "jiangwei124",
    "user_name": "jiangwei124",
    "url": "https://gitee.com/jiangwei124",
    "login": "jiangwei124",
    "avatar_url": "https://portrait.gitee.com/uploads/avatars/user/3212/9637903_jiangwei124_1629964158.png",
    "html_url": "https://gitee.com/jiangwei124",
    "type": "User",
    "site_admin": False
  },
  "assignee": None,
  "updated_by": {
    "id": 9637903,
    "name": "jiangwei",
    "email": "qq804022023@gmail.com",
    "username": "jiangwei124",
    "user_name": "jiangwei124",
    "url": "https://gitee.com/jiangwei124",
    "login": "jiangwei124",
    "avatar_url": "https://portrait.gitee.com/uploads/avatars/user/3212/9637903_jiangwei124_1629964158.png",
    "html_url": "https://gitee.com/jiangwei124",
    "type": "User",
    "site_admin": False
  },
  "iid": "I480IG",
  "title": "测试一下webhook的issue",
  "description": "测试一下",
  "state": "open",
  "milestone": None,
  "url": "https://gitee.com/xueyang721/test-webhook/issues/I480IG",
  "enterprise": None,
  "hook_name": "issue_hooks",
  "hook_id": 736791,
  "hook_url": "https://gitee.com/xueyang721/test-webhook/hooks/736791/edit",
  "password": "",
  "timestamp": "1630396733948",
  "sign": "zERyUciGFQ/GFG1thY2aXohBC96orx6JVOP0kTjr89Y="
}

send_greeting_for_issue(issue)
