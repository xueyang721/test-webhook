# test-webhook

#### 介绍
测试webhook

#### 软件架构
使用flask编写的Gitee WebHook以及Gitee API的相关测试接口。


#### 安装教程

使用如下命令安装

```bash
git clone 
cd test-webhook
pip install -r requirements.txt
flask run
```

> 服务需要部署在公网服务器中才能有效的跟Gitee WebHook结合起来使用。

#### webhook远程服务URL

1. http://106.13.18.133:8080/web-hook/test-handler

    测试WebHook的功能，没有实际的作用
2. http://106.13.18.133:8080/web-hook/pr-handler

    处理pr相关的WebHook，目前只支持open以及merge相关的动作
3. http://106.13.18.133:8080/web-hook/issue-handler

    处理issue相关的WebHook，目前只支持open动作



